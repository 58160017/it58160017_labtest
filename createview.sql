CREATE VIEW StudentAdvise AS
SELECT s.StudentID, s.StudentName, s.StudentSurname AS Student,
       m.DeptName AS Department,
	a.AdvisorName, a.AdvisorSurname AS Advisor
FROM Student AS s
LEFT JOIN (Department AS m, Advisor AS a)
ON ( m.DeptID = s.DeptID AND a.AdvisorID = s.AdvisorID);
